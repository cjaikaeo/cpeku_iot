Blockly.Msg.CPEKU_MQTT_CONNECT = "connect to MQTT broker";
Blockly.Msg.CPEKU_MQTT_PORT = "port";
Blockly.Msg.CPEKU_MQTT_USER = "with username";
Blockly.Msg.CPEKU_MQTT_PASS = "and password";
Blockly.Msg.CPEKU_MQTT_CONNECT_TOOLTIP = "connect to an MQTT broker";
Blockly.Msg.CPEKU_MQTT_CONNECT_USER_TOOLTIP = "connect to an MQTT broker with user account";

Blockly.Msg.CPEKU_MQTT_PUBLISH = "publish to topic";
Blockly.Msg.CPEKU_MQTT_WITH_MSG = "with message";
Blockly.Msg.CPEKU_MQTT_PUBLISH_TOOLTIP = "publish a message with the specified topic";

Blockly.Msg.CPEKU_MQTT_ON_RECV = "on MQTT receive topic";
Blockly.Msg.CPEKU_MQTT_ON_RECV_TOOLTIP = "subscribe to the given topic and register a handler when data arrives";

Blockly.Msg.CPEKU_MQTT_CONNECTED = "MQTT is connected";
Blockly.Msg.CPEKU_MQTT_CONNECTED_TOOLTIP = "check if MQTT client is connected to the broker";

Blockly.Msg.CPEKU_MQTT_MSG_TEXT = "received text";
Blockly.Msg.CPEKU_MQTT_MSG_TEXT_TOOLTIP = "returns latest received data as text";

Blockly.Msg.CPEKU_MQTT_MSG_NUMBER = "received number";
Blockly.Msg.CPEKU_MQTT_MSG_NUMBER_TOOLTIP = "returns latest received data as number";

Blockly.Msg.CPEKU_MQTT_IP_ADDR = "IP address";
Blockly.Msg.CPEKU_MQTT_IP_ADDR_TOOLTIP = "returns device's IP address, or 0.0.0.0 if not available";
