#ifndef __CPEKU_MQTT_CLIENT_H__
#define __CPEKU_MQTT_CLIENT_H__

#include <map>
#include "driver.h"
#include "device.h"
#include "esp_log.h"
#include "mqtt_client.h"

class CPEKU_MQTT : public Device {
  public:
    typedef void(*OnMessageCallback)();
    typedef std::map<std::string,OnMessageCallback> CallbackMap;

    CPEKU_MQTT();

    void init(void) override;
    void process(Driver *drv) override;
    int prop_count(void) override { return 0; }
    bool prop_name(int index, char *name) override { return false; }
    bool prop_unit(int index, char *unit) override { return false; }
    bool prop_attr(int index, char *attr) override { return false; }
    bool prop_read(int index, char *value) override { return false; }
    bool prop_write(int index, char *value) override { return false; }

    bool connected() const { return _state == CONNECTED; }
    void connect(const char* host, uint16_t port,
                 char* user, char* passwd);
    void connect(const char* host, uint16_t port);
    void publish(const char* topic, const char* msg);
    void publish(const char* topic, double msg);
    void on_message(const char* topic, OnMessageCallback cb);
    char* received_text();
    double received_number();
    char* ip_addr();

  private:
    enum state_t {
      DETECT,
      CONNECTED,
      DISCONNECTED,
    };
    static state_t _state;
    static char _ip_addr[32];
    static esp_mqtt_client_handle_t _client;
    static esp_err_t _event_handler(esp_mqtt_event_handle_t event);
    static void _handle_data(char* topic, int topic_len, char* data, int data_len);
    static CallbackMap _cbmap;
    static char _received_text[];
};

#endif
