Blockly.JavaScript['cpeku_mqtt_publish'] = function(block) {
  var text_topic = block.getFieldValue('topic');
  var value_msg = Blockly.JavaScript.valueToCode(block, 'msg', Blockly.JavaScript.ORDER_ATOMIC);
  var code = 'DEV_IO.CPEKU_MQTT().publish(' 
           + '"' + text_topic + '"'
           + ',' + value_msg
           + ');\n';
  return code;
};

Blockly.JavaScript['cpeku_mqtt_on_message'] = function(block) {
  var text_topic = block.getFieldValue('topic');
  var statements_handler = Blockly.JavaScript.statementToCode(block, 'handler');
  var code = 'DEV_IO.CPEKU_MQTT().on_message(' 
           + '"' + text_topic + '"'
           + ',[]() {\n'
           + statements_handler
           + '});\n';
  return code;
};

Blockly.JavaScript['cpeku_mqtt_connect'] = function(block) {
  var text_host = block.getFieldValue('host');
  var text_port = block.getFieldValue('port');
  var code = 'DEV_IO.CPEKU_MQTT().connect(' 
           + '"' + text_host + '"'
           + ',' + text_port
           + ');\n';
  return code;
};

Blockly.JavaScript['cpeku_mqtt_connect_user'] = function(block) {
  var text_host = block.getFieldValue('host');
  var text_port = block.getFieldValue('port');
  var text_username = block.getFieldValue('username');
  var text_password = block.getFieldValue('password');
  var code = 'DEV_IO.CPEKU_MQTT().connect(' 
           + '"' + text_host + '"'
           + ',' + text_port
           + ',"' + text_username + '"'
           + ',"' + text_password + '"'
           + ');\n';
  return code;
};

Blockly.JavaScript['cpeku_mqtt_is_connected'] = function(block) {
  var code = 'DEV_IO.CPEKU_MQTT().connected()';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['cpeku_mqtt_msg_text'] = function(block) {
  var code = 'DEV_IO.CPEKU_MQTT().received_text()';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['cpeku_mqtt_msg_number'] = function(block) {
  var code = 'DEV_IO.CPEKU_MQTT().received_number()';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['cpeku_mqtt_ip_addr'] = function(block) {
  var code = 'DEV_IO.CPEKU_MQTT().ip_addr()';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};
