Blockly.Blocks['cpeku_mqtt_connect'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(Blockly.Msg.CPEKU_MQTT_CONNECT)
        .appendField(new Blockly.FieldTextInput("some.broker.org"), "host")
        .appendField(Blockly.Msg.CPEKU_MQTT_PORT)
        .appendField(new Blockly.FieldTextInput("1883"), "port");
    this.appendDummyInput();
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(195);
 this.setTooltip(Blockly.Msg.CPEKU_MQTT_CONNECT_TOOLTIP);
 this.setHelpUrl("");
  }
};

Blockly.Blocks['cpeku_mqtt_connect_user'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(Blockly.Msg.CPEKU_MQTT_CONNECT)
        .appendField(new Blockly.FieldTextInput("some.broker.org"), "host")
        .appendField(Blockly.Msg.CPEKU_MQTT_PORT)
        .appendField(new Blockly.FieldTextInput("1883"), "port");
    this.appendDummyInput()
        .appendField(Blockly.Msg.CPEKU_MQTT_USER)
        .appendField(new Blockly.FieldTextInput("username"), "username")
        .appendField(Blockly.Msg.CPEKU_MQTT_PASS)
        .appendField(new Blockly.FieldTextInput("password"), "password");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(195);
 this.setTooltip(Blockly.Msg.CPEKU_MQTT_CONNECT_USER_TOOLTIP);
 this.setHelpUrl("");
  }
};

Blockly.Blocks['cpeku_mqtt_publish'] = {
  init: function() {
    this.appendValueInput("msg")
        .setCheck(["String", "Number"])
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField(Blockly.Msg.CPEKU_MQTT_PUBLISH)
        .appendField(new Blockly.FieldTextInput("topic"), "topic")
        .appendField(Blockly.Msg.CPEKU_MQTT_WITH_MSG);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(195);
 this.setTooltip(Blockly.Msg.CPEKU_MQTT_PUBLISH_TOOLTIP);
 this.setHelpUrl("");
  }
};

Blockly.Blocks['cpeku_mqtt_on_message'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(Blockly.Msg.CPEKU_MQTT_ON_RECV)
        .appendField(new Blockly.FieldTextInput("topic"), "topic");
    this.appendStatementInput("handler")
        .setCheck(null);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(195);
 this.setTooltip(Blockly.Msg.CPEKU_MQTT_ON_RECV_TOOLTIP);
 this.setTooltip("Subscribe to the given topic and register a handler when data arrives");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['cpeku_mqtt_is_connected'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(Blockly.Msg.CPEKU_MQTT_CONNECTED);
    this.setOutput(true, "Boolean");
    this.setColour(195);
 this.setTooltip(Blockly.Msg.CPEKU_MQTT_CONNECTED_TOOLTIP);
 this.setHelpUrl("");
  }
};

Blockly.Blocks['cpeku_mqtt_msg_text'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(Blockly.Msg.CPEKU_MQTT_MSG_TEXT);
    this.setOutput(true, "String");
    this.setColour(195);
 this.setTooltip(Blockly.Msg.CPEKU_MQTT_MSG_TEXT_TOOLTIP);
 this.setHelpUrl("");
  }
};

Blockly.Blocks['cpeku_mqtt_msg_number'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(Blockly.Msg.CPEKU_MQTT_MSG_NUMBER);
    this.setOutput(true, "Number");
    this.setColour(195);
 this.setTooltip(Blockly.Msg.CPEKU_MQTT_MSG_NUMBER_TOOLTIP);
 this.setHelpUrl("");
  }
};

Blockly.Blocks['cpeku_mqtt_ip_addr'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(Blockly.Msg.CPEKU_MQTT_IP_ADDR);
    this.setOutput(true, "String");
    this.setColour(195);
 this.setTooltip(Blockly.Msg.CPEKU_MQTT_IP_ADDR_TOOLTIP);
 this.setHelpUrl("");
  }
};
